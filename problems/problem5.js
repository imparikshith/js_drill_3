const inventory = require("../data.js");

function problem5() {
  try {
    let yearsOlderThan2000 = [];
    for (let record of inventory) {
      if (record.car_year < 2000) {
        yearsOlderThan2000.push(record.car_year);
      }
    }
    console.log(yearsOlderThan2000);
  } catch (error) {
    console.error("An error occurred:", error.message);
  }
}
module.exports = problem5;