const inventory = require("../data.js");

function problem1() {
  try {
    for (let record of inventory) {
      if (record.id == 33) {
        console.log(
          `Car 33 is a ${record.car_year} ${record.car_make} ${record.car_model}`
        );
      }
    }
  } catch (error) {
    console.error("An error occurred:", error.message);
  }
}
module.exports = problem1;