const inventory = require("../data.js");

function problem6() {
  try {
    let BMWAndAudi = [];
    for (let record of inventory) {
      if (record.car_make == "BMW" || record.car_make == "Audi") {
        BMWAndAudi.push(JSON.stringify(record));
      }
    }
    console.log(BMWAndAudi);
  } catch (error) {
    console.error("An error occurred:", error.message);
  }
}
module.exports = problem6;