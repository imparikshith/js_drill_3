const inventory = require("../data.js");

function problem2() {
  try {
    let record;
    for (record of inventory) {
    }
    console.log(`Last car is a ${record.car_make} ${record.car_model}`);
  } catch (error) {
    console.error("An error occurred:", error.message);
  }
}
module.exports = problem2;