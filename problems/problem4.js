const inventory = require("../data.js");

function problem4() {
  try {
    let years = [];
    for (let record of inventory) {
      years.push(record.car_year);
    }
    console.log(years);
  } catch (error) {
    console.error("An error occurred:", error.message);
  }
}
module.exports = problem4;