const inventory = require("../data.js");

function problem3() {
  try {
    let carModels = [];
    for (let record of inventory) {
      carModels.push(record.car_model.toUpperCase());
    }
    carModels.sort();
    console.log(carModels);
  } catch (error) {
    console.error("An error occurred:", error.message);
  }
}
module.exports = problem3;